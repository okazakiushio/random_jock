function getRandomJoke() {
    fetch('jokes.json')
    .then(response => response.json())
    .then(data => {
        const jokes = data.jokes;
        const randomIndex = Math.floor(Math.random() * jokes.length);
        const joke = jokes[randomIndex].text;
        document.getElementById("jokeText").innerHTML = joke;
    })
    .catch(error => console.error('讀取笑話出錯:', error));
}
